package com.zhenjiashi.sideline;

import com.zhenjiashi.sideline.service.IJobsService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
class SidelineApplicationTests {

    @Autowired
    private IJobsService jobsService;

    @Test
    void test() {
        log.info(jobsService.count()+"");
    }

}
