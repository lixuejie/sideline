<!DOCTYPE html>
<html lang="zh">
<head>
    <META http-equiv=Content-Type content='text/html; charset=UTF-8'>
    <title>Title</title>
    <style type="text/css">
        table.reference, table.tecspec {
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 4px;
            margin-top: 4px;
        }

        table.reference tr:nth-child(even) {
            background-color: #fff;
        }

        table.reference tr:nth-child(odd) {
            background-color: #f6f4f0;
        }

        table.reference th {
            color: #fff;
            background-color: #0dcde8;
            border: 1px solid #555;
            font-size: 12px;
            padding: 3px;
            vertical-align: center;
        }

        table.reference td {
            line-height: 2em;
            min-width: 24px;
            border: 1px solid #d4d4d4;
            padding: 5px;
            padding-top: 7px;
            padding-bottom: 7px;
            vertical-align: center;
            text-align: center;
        }

        .article-body h3 {
            font-size: 1.8em;
            margin: 2px 0;
            line-height: 1.8em;
        }

        .active {
            font-size: 18px;
            background-color: #0dcde8;
        }

        #nav {
            height: 65px;
            border-top: #060 2px solid;
            border-bottom: #060 2px solid;
            background-color: #8a858566;
        }

        #nav ul {
            list-style: none;
            margin-left: 50px;
        }

        #nav li {
            display: inline;
            line-height: 40px;
            float: left
        }

        #nav a {
            color: #0a0a0a;
            text-decoration: none;
            padding: 20px 20px;
        }

        #nav a:hover {
            background-color: #060;
        }
    </style>
</head>
<body>

<div>
    <table class="reference">
        <tbody>

        <tr>
            <th>创建时间</th>
            <th>发布平台</th>
            <th>标题</th>
            <th>描述</th>
            <th>价格/元</th>
            <th>工期/天</th>

        </tr>
        <#list page.records as element>
            <tr>
                <td>
                    ${(element.publishTime)?string("yyyy-MM-dd HH:mm:ss")}
                </td>
                <td>
                    ${element.platform}
                </td>
                <td>
                    <a href="${element.url}">${element.title}</a>
                </td>
                <td style="width: 50%;">
                    ${element.content}
                </td>
                <td>
                    ${element.price}
                </td>
                <td>
                    ${element.days}
                </td>

            </tr>
        </#list>
        <br>
        </tbody>
    </table>
    <div class="message">
        共<i class="blue">${page.total}</i>条记录，当前显示第&nbsp;<i
                class="blue">${page.current}/${page.pages}</i>&nbsp;页
    </div>
    <div style="text-align:center;" id="nav">
        <ul>
            <#if page.current != 1>
                <li><a href="./index">首页</a></li>
                <li><a href="./index?current=${page.current-1}">上一页</a></li>
            </#if>
<#--            <#list page.navigatecurrents as element>-->
<#--                <#if element==page.current>-->
<#--                    <li class="active"><a href="./index?current=${element}">${element}</a></li>-->
<#--                </#if>-->
<#--                <#if element!=page.current>-->
<#--                    <li><a href="./index?current=${element}">${element}</a></li>-->
<#--                </#if>-->
<#--            </#list>-->

            <#if page.current!=page.pages>
                <li><a href="./index?current=${page.current+1}">下一页</a></li>
                <li><a href="./index?current=${page.pages}">最后一页</a></li>
            </#if>
        </ul>
    </div>
</div>
</body>
</html>
