package com.zhenjiashi.sideline;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
@MapperScan("com.zhenjiashi.sideline.mapper")
public class SidelineApplication {

    public static void main(String[] args) {
        SpringApplication.run(SidelineApplication.class, args);
    }

}
