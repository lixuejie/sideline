package com.zhenjiashi.sideline.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhenjiashi.sideline.entity.Jobs;
import com.zhenjiashi.sideline.mapper.JobsMapper;
import com.zhenjiashi.sideline.service.IJobsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zhenjiashi
 * @since 2022-03-09
 */
@Service
public class JobsServiceImpl extends ServiceImpl<JobsMapper, Jobs> implements IJobsService {

    @Override
    public IPage<Jobs> selectPage(String platform, String title, String content, Long current, Long size) {
        Page<Jobs> page = new Page<>(current, size);

        LambdaQueryWrapper<Jobs> qw = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(platform)) {
            qw.eq(Jobs::getPlatform, platform);
        }
        if (StringUtils.isNotBlank(title)) {
            qw.like(Jobs::getTitle, title);
        }
        if (StringUtils.isNotBlank(content)) {
            qw.like(Jobs::getContent, content);
        }
        qw.orderByDesc(Jobs::getPublishTime);
//        page.addOrder(new OrderItem("publish_time", false));
        return this.baseMapper.selectPage(page, qw);
    }
}
