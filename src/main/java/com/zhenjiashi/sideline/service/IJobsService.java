package com.zhenjiashi.sideline.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhenjiashi.sideline.entity.Jobs;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhenjiashi
 * @since 2022-03-09
 */
public interface IJobsService extends IService<Jobs> {

    IPage<Jobs> selectPage(String platform, String title, String content, Long current, Long size);
}
