package com.zhenjiashi.sideline.mapper;

import com.zhenjiashi.sideline.entity.Jobs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhenjiashi
 * @since 2022-03-09
 */
public interface JobsMapper extends BaseMapper<Jobs> {

}
