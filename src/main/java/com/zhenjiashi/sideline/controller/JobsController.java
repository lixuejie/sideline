package com.zhenjiashi.sideline.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhenjiashi.sideline.entity.Jobs;
import com.zhenjiashi.sideline.service.IJobsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Objects;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zhenjiashi
 * @since 2022-03-09
 */
@Controller
@RequestMapping("/")
public class JobsController {

    @Autowired
    private IJobsService jobsService;

    @ResponseBody
    @GetMapping(value = "/jobs/page")
    public IPage<Jobs> page(String platform, String title, String content, Long current, Long size) {
        return jobsService.selectPage(platform, title, content, current, size);
    }

    @RequestMapping("/index")
    public String show(Map<String, Object> map,
                       @RequestParam(value = "platform", required = false) String platform,
                       @RequestParam(value = "title", required = false) String title,
                       @RequestParam(value = "content", required = false) String content,
                       @RequestParam(value = "current", required = false, defaultValue = "1") Long current,
                       @RequestParam(value = "size", required = false, defaultValue = "20") Long size) {
        if (Objects.isNull(current)) {
            current = 1L;
        }
        if (Objects.isNull(size)) {
            size = 20L;
        }
        IPage<Jobs> page = jobsService.selectPage(platform, title, content, current, size);
        map.put("page", page);
        return "index";
    }

}
