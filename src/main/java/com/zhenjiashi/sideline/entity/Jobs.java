package com.zhenjiashi.sideline.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 *
 * </p>
 *
 * @author zhenjiashi
 * @since 2022-03-09
 */
@Data
@ApiModel(value = "Jobs对象", description = "")
public class Jobs implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date createTime;

    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date updateTime;

    @ApiModelProperty("发布时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date publishTime;

    @ApiModelProperty("发布平台")
    private String platform;

    @ApiModelProperty("链接")
    private String url;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("标签")
    private String tags;

    @ApiModelProperty("内容描述")
    private String content;

    @ApiModelProperty("价格")
    private BigDecimal price;

    @ApiModelProperty("工期")
    private Long days;

}
