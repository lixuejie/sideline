package com.zhenjiashi.sideline.cralwer.codemart;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.zhenjiashi.sideline.cralwer.DataPipeLine;
import com.zhenjiashi.sideline.cralwer.codemart.model.Reward;
import com.zhenjiashi.sideline.entity.Jobs;
import com.zhenjiashi.sideline.service.IJobsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.scheduler.BloomFilterDuplicateRemover;
import us.codecraft.webmagic.scheduler.QueueScheduler;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Component
public class CodemartScheduler {

    @Autowired
    private DataPipeLine dataPipeLine;

    private static final String path = "https://codemart.com/api/project";

    @Scheduled(cron = "0 0/1 * * * ?")
    public void execute() {
        String result = HttpUtil.get(path);
        if (StringUtils.isNoneBlank(result)) {
            JSONObject jsonObject = JSON.parseObject(result);
            if (Objects.nonNull(jsonObject)) {
                List<Reward> rewards = JSON.parseArray(jsonObject.getString("rewards"), Reward.class);
                if (!CollectionUtils.isEmpty(rewards)) {
                    List<Jobs> jobs = rewards.stream().map(reward -> {
                        Jobs job = new Jobs();
                        job.setCreateTime(new Date());
                        job.setPlatform("码市");
                        job.setUrl("https://codemart.com/project/" + reward.getId());
                        job.setTitle(reward.getName());
                        job.setContent(reward.getDescription());
                        job.setPrice(new BigDecimal(reward.getPrice()));
                        job.setDays(reward.getDuration().longValue());
                        job.setPublishTime(new Date(Long.parseLong(reward.getPubTime())));
                        job.setTags(reward.getTypeText());
                        return job;
                    }).collect(Collectors.toList());

                    dataPipeLine.saveAll(jobs);
                }
            }
        }
    }

}
