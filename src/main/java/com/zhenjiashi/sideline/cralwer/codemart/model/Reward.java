package com.zhenjiashi.sideline.cralwer.codemart.model;
import lombok.Data;

import java.util.List;

@Data
public class Reward {

    private long ownerId;
    private String name;
    private String description;
    private String cover;
    private String price;
    private String roles;
    private String type;
    private String status;
    private String pubTime;
    private Integer duration;
    private Integer applyCount;
    private Integer id;
    private String createdAt;
    private String developerType;
    private boolean bargain;
    private String statusText;
    private String typeText;
    private Integer visitCount;
    private List<Types> types;
    private String specificRole;
    private List<Labels> labels;

}
