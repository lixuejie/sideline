package com.zhenjiashi.sideline.cralwer.codemart.model;

import lombok.Data;

@Data
public class Types {

    private int id;
    private String type;
    private String name;

}
