package com.zhenjiashi.sideline.cralwer.codemart.model;

import lombok.Data;

@Data
public class Labels {

    private int id;
    private String label;
    private String color;
    private int labelId;
    private String description;

}
